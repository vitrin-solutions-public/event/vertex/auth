<?php

namespace Vitrin\Event\Auth\User;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserLoggedOut extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}
