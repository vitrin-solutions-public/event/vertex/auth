<?php

namespace Vitrin\Event\Auth\User;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserCreated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}
