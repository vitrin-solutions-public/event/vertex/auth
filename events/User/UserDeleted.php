<?php

namespace Vitrin\Event\Auth\User;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserDeleted extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
    ) {
        //
    }
}
