<?php

namespace Vitrin\Event\Auth\User;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserUpdated extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public int|string $id,
        public array $properties = [],
    ) {
        //
    }
}
