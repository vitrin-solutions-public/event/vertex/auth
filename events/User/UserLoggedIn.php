<?php

namespace Vitrin\Event\Auth\User;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class UserLoggedIn extends ShouldBeStored
{
    /**
     * Create a new event instance.
     */
    public function __construct(
        public array $properties = [],
    ) {
        //
    }
}
